import React, {useState, useEffect} from 'react';

import styles from './Positive.module.scss';

const Positive = () => {
  const [comments, setComments] = useState([]);

  const fetchComments = async () => {
    const response = await fetch('/api/comments/1', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    const body = await response.text();
    const data = JSON.parse(body);

    setComments(data.comments);
  }

  useEffect(()=>{
    fetchComments();
  }, []);

  return (
    <div className={styles.positive}>            
      {comments.map((comment, index) => {
        return (
          <div key={index} className={styles.comment}>
            <span>{comment.text}</span>
          </div>
        );
      })}
    </div>
  );
}

export default Positive;