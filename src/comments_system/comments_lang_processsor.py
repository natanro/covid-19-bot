'''
Processamento de Linguagem Natual de comentários.
Objetivo é mandar para o Classifier o dataset de comentários.
'''

from nltk import stem, corpus
from nltk.tokenize import word_tokenize
from joblib import load, dump

from src.models.comments import Comment


class CommentsLanguageProcessor:

    __translator = {}

    def __init__(self, comments: Comment, marca: str):
        self.__marca = marca
        self.__comments = comments
        try:
            CommentsLanguageProcessor.__translator = load(self.__marca+'.translator.CommentsLanguageProcessor.model')
        except :
            pass

    def comments_vectorized(self, language='portuguese'):
        try:
            broken_comments = self.__comments.get_broken_comments()
        except:
            return None
        
        words_dict = set()
        stopwords = corpus.stopwords.words(language)
        stemmer = self.__get_stemmer(language)

        for comment in broken_comments:
            valid_words = [stemmer.stem(
                word) for word in comment if word not in stopwords and len(word) > 2]
            words_dict.update(valid_words)

        indices = range(len(words_dict))
        translator = self.__get_translator(words_dict, indices)

        return [self.__vectorize_comment(
            comment, translator, stemmer) for comment in broken_comments]

    def __vectorize_comment(self, comment, translator, stemmer):
        comments_vector = [0] * len(translator)

        for word in comment:
            if stemmer.stem(word) in translator:
                pos = translator[stemmer.stem(word)]
                comments_vector[pos] += 1

        return comments_vector

    def get_labels(self):
        return self.__comments.get_labels()

    def __get_stemmer(self, language):
        stemmer = None
        if language == 'english':
            stemmer = stem.SnowballStemmer(language)
        elif language == 'portuguese':
            stemmer = stem.RSLPStemmer()
        return stemmer

    def __get_translator(self, words_dict, indices):
        if len(CommentsLanguageProcessor.__translator) == 0:
            CommentsLanguageProcessor.__translator = {
                word: index for word, index in zip(words_dict, indices)}
            dump(CommentsLanguageProcessor.__translator, self.__marca+'.translator.CommentsLanguageProcessor.model')
        return CommentsLanguageProcessor.__translator
