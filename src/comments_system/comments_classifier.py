'''
Armazena os classificadores. Objetivo é devolver um vetor de labels (positivo ou negativo)
para cada comentário.
'''

from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from joblib import dump, load


class CommentsClassifier:

    def __init__(self, persistence=False):
        self.__classifiers = [Classifier('MultinomialNB', persistence=persistence),
                              Classifier('SGDClassifier',
                                         persistence=persistence),
                              Classifier('MLPClassifier', persistence=persistence)]
        self.__classifiers_params = {}

    def classify_comments(self, comments):
        predictions = []
        if(len(self.__classifiers_params) == 0):
            self.__classifiers_params = load('classifiers_param.comm')
        for classifier in self.__classifiers:
            prediction = classifier.predict(comments)
            predictions.append((classifier.get_classifier_name(), prediction))
        
        return __final_classification(predictions)

    def train_comments(self, comments, classification, verbose=False):
        for classifier in self.__classifiers:
            try:
                spec, sens = classifier.train(comments, classification, verbose=verbose)
                self.__classifiers_params[classifier.get_classifier_name()] = (spec, sens)
            except ValueError as vl:
                raise ValueError()
        dump(self.__classifiers_params, 'classifiers_param.comm')
        return 0

    def get_classification(self):
        return self.__classification

    def __final_classification(self, predictions: list):
        test_classification = [(1,1)]*len(predictions[0][1])
        for pred in predictions:
            for i,cl in enumerate(pred):
                test_classification[i][cl] *= (1-self.__classifiers_params[pred[0]][cl])

        final_classification = [0]*len(predictions[0][1])
        for i,tc in enumerate(test_classification):
            if tc[0] > tc[1]:
                final_classification[i] = 1
                
        return final_classification



class Classifier:

    def __init__(self, model: str, persistence=False):
        filename_persistence = {
            'MultinomialNB': 'MultinomialNBClassifier.model',
            'SGDClassifier': 'SGDClassifier.model',
            'MLPClassifier': 'MLPClassifier.model'}
        self.__filename_persistence = filename_persistence[model]
        self.__name = model
        classifiers = {'MultinomialNB': MultinomialNB(),
                       'SGDClassifier': SGDClassifier(),
                       'MLPClassifier': MLPClassifier()}
        if persistence:
            self.__classifier = load(self.__filename_persistence[model])
        else:
            self.__classifier = classifiers[model]

    def train(self, comments, classification, verbose=False):
        X_train, X_test, y_train, y_test = train_test_split(
            comments, classification, test_size=0.2)

        self.__classifier.fit(X_train, y_train)
        y_pred = self.__classifier.predict(X_test)
        report = classification_report(y_test, y_pred, labels=[0, 1],target_names=[
                                       'negativo', 'positivo'], output_dict=True)

        filename = dump(self.__classifier, self.__filename_persistence)
        if verbose:
            print('Model trained: '+self.__name)
            print(report)
            print('Model saved in '+str(filename))

        return report['negativo']['recall'], report['positivo']['recall']

    def predict(self, comments):
        return self.__classifier.predict(comments)

    def get_classifier_name(self):
        return self.__name