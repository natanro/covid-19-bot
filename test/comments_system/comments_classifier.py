import unittest

from src.comments_system.comments_classifier import CommentsClassifier

class CommentsClassifierTest(unittest.TestCase):
    
    def test_classe_equivalencia_invalida(self):
        with self.assertRaises(ValueError):
            CommentsClassifier().train_comments([], [])
            CommentsClassifier().train_comments([1,1,0,1], [1])

    def test_classe_equivalencia_valida(self):
        ret_multiple_inputs = CommentsClassifier().train_comments([[1,1,0,2,0], [1,0,0,2,1], [1,1,1,0,1], [1,1,0,2,0], [1,0,0,2,1], [1,1,1,0,1]], [1, 1, 0,1, 1, 0])
        self.assertEquals(ret_multiple_inputs, 0)

