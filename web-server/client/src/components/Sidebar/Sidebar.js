import React from 'react';
import {NavLink} from 'react-router-dom';

import styles from './Sidebar.module.scss';

const Sidebar = ({setIsAuth}) => {
  return (
    <div className={styles.sidebar}>            
      <NavLink to='/percentage' activeClassName={styles.active}>
        Porcentagem
      </NavLink>
      <NavLink to='/positive' activeClassName={styles.active}>
        Comentários Positivos
      </NavLink>
      <NavLink to='/negative' activeClassName={styles.active}>
        Comentários negativos
      </NavLink>
      <div className={styles.fill} />
      <div className={styles.logout} >
        <button type="button" id="logout" onClick={() => {setIsAuth(false); localStorage.setItem('isAuth', false)}}>
          Sair
        </button>
      </div>
    </div>
  );
}

export default Sidebar;