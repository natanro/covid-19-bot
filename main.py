import pandas as pd
from nltk import stem, corpus
from nltk.tokenize import word_tokenize
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from joblib import dump, load

from src.models.comments import Comment
# from src.comments_system.comments_lang_processor import CommentsLanguageProcessor


# print(raw_comments)

def get_comment_object(raw_comments):
    comm_raw_dict = {}
    comm_raw_dict['text'] = [comm for comm in raw_comments['comment']]
    comm_raw_dict['classification'] = [l for l in raw_comments['label']]
    # print(comm_raw_dict)
    return Comment(comm_raw_dict)
    # print(a.get_broken_comments())


class CommentsLanguageProcessor:

    __translator = {}

    def __init__(self, comments: Comment, marca: str):
        self.__marca = marca
        self.__comments = comments
        try:
            CommentsLanguageProcessor.__translator = load(self.__marca+'.translator.CommentsLanguageProcessor.model')
            print('PEGOOOOOOU')
        except :
            pass

    def comments_vectorized(self, language='portuguese'):
        try:
            broken_comments = self.__comments.get_broken_comments()
        except:
            return None
        
        words_dict = set()
        stopwords = corpus.stopwords.words(language)
        stemmer = self.__get_stemmer(language)

        for comment in broken_comments:
            valid_words = [stemmer.stem(
                word) for word in comment if word not in stopwords and len(word) > 2]
            words_dict.update(valid_words)

        indices = range(len(words_dict))
        translator = self.__get_translator(words_dict, indices)

        return [self.__vectorize_comment(
            comment, translator, stemmer) for comment in broken_comments]

    def __vectorize_comment(self, comment, translator, stemmer):
        comments_vector = [0] * len(translator)

        for word in comment:
            if stemmer.stem(word) in translator:
                pos = translator[stemmer.stem(word)]
                comments_vector[pos] += 1

        return comments_vector

    def get_labels(self):
        return self.__comments.get_labels()

    def __get_stemmer(self, language):
        stemmer = None
        if language == 'english':
            stemmer = stem.SnowballStemmer(language)
        elif language == 'portuguese':
            stemmer = stem.RSLPStemmer()
        return stemmer

    def __get_translator(self, words_dict, indices):
        if len(CommentsLanguageProcessor.__translator) == 0:
            CommentsLanguageProcessor.__translator = {
                word: index for word, index in zip(words_dict, indices)}
            dump(CommentsLanguageProcessor.__translator, self.__marca+'.translator.CommentsLanguageProcessor.model')
        return CommentsLanguageProcessor.__translator


def get_vect_comms_and_classification(comm):
    comm_lang_proc = CommentsLanguageProcessor(comm, 'nubank')
    return comm_lang_proc.comments_vectorized(language='portuguese'), comm_lang_proc.get_labels()


class CommentsClassifier:

    def __init__(self, persistence=False):
        self.__classifiers = [Classifier('MultinomialNB', persistence=persistence),
                              Classifier('SGDClassifier',
                                         persistence=persistence),
                              Classifier('MLPClassifier', persistence=persistence)]
        self.__classifiers_params = {}

    def classify_comments(self, comments):
        predictions = []
        if(len(self.__classifiers_params) == 0):
            self.__classifiers_params = load('classifiers_param.comm')
        for classifier in self.__classifiers:
            prediction = classifier.predict(comments)
            predictions.append((classifier.get_classifier_name(), prediction))
        print(predictions)
        return self.__final_classification(predictions)

    def train_comments(self, comments, classification, verbose=False):
        for classifier in self.__classifiers:
            try:
                spec, sens = classifier.train(comments, classification, verbose=verbose)
                self.__classifiers_params[classifier.get_classifier_name()] = (spec, sens)
            except ValueError as vl:
                raise ValueError()
        dump(self.__classifiers_params, 'classifiers_param.comm')
        return 0

    def get_classification(self):
        return self.__classification

    def __final_classification(self, predictions: list):
        test_classification = predictions[0][1]+predictions[1][1]+predictions[2][1]
        final_classification = [0] * len(test_classification)
        for i,tc in enumerate(test_classification):
            if tc >= 2:
                final_classification[i] = 1
                
        return final_classification

class Classifier:

    def __init__(self, model: str, persistence=False):
        filename_persistence = {
            'MultinomialNB': 'MultinomialNBClassifier.model',
            'SGDClassifier': 'SGDClassifier.model',
            'MLPClassifier': 'MLPClassifier.model'}
        self.__filename_persistence = filename_persistence[model]
        self.__name = model
        classifiers = {'MultinomialNB': MultinomialNB(),
                       'SGDClassifier': SGDClassifier(),
                       'MLPClassifier': MLPClassifier()}
        if persistence:
            self.__classifier = load(self.__filename_persistence)
        else:
            self.__classifier = classifiers[model]

    def train(self, comments, classification, verbose=False):
        X_train, X_test, y_train, y_test = train_test_split(
            comments, classification, test_size=0.2)

        self.__classifier.fit(X_train, y_train)
        y_pred = self.__classifier.predict(X_test)
        report = classification_report(y_test, y_pred, labels=[0, 1],target_names=[
                                       'negativo', 'positivo'], output_dict=True)

        filename = dump(self.__classifier, self.__filename_persistence)
        if verbose:
            print('Model trained: '+self.__name)
            print(report)
            print('Model saved in '+str(filename))

        return report['negativo']['recall'], report['positivo']['recall']

    def predict(self, comments):
        return self.__classifier.predict(comments)

    def get_classifier_name(self):
        return self.__name


def create_translator_and_get_vect_comms(file):
    raw_comments = pd.read_csv(file, error_bad_lines=False)
    comment = get_comment_object(raw_comments)
    return get_vect_comms_and_classification(comment) 

def train_classifiers():
    comms_vect, classification = create_translator_and_get_vect_comms('comm.csv')
    comm_classifier = CommentsClassifier()
    comm_classifier.train_comments(comms_vect, classification, verbose=True)

def classify():
    comms_vect, classification = create_translator_and_get_vect_comms('commToClass.csv')
    comm_classifier = CommentsClassifier(persistence=True)
    return comm_classifier.classify_comments(comms_vect)

import psycopg2
from time import sleep

def send_comms_to_db(class_final):
    comms = pd.read_csv('commToClass.csv', error_bad_lines=False)['comment']
    final = [comms, class_final]

    con = psycopg2.connect(host='twitterbot.ccptai2yvrbu.us-east-2.rds.amazonaws.com', 
        database='twitterbot', user='postgres', password='gertrudes123', port=5432)
    cur = con.cursor()
    for i, comm in enumerate(final[0]):
        cur.execute("INSERT INTO tweets (text, classification) VALUES (%s, %s)",(str(final[0][i]), final[1][i]))
        # print("comm: "+str(final[0][i])+" class: "+str(final[1][i]))
        sleep(1)
    con.commit()
    cur.close()
    con.close()


def main():
    train_classifiers()
    class_final = classify()
    send_comms_to_db(class_final)