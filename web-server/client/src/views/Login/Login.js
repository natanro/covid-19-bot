import React, { useState } from 'react';

import styles from './Login.module.scss';

const Login = ({setIsAuth}) => {
    const [ forms, setForms ] = useState({username: '', password: ''});

    const handleChange = (e) => {
        const { name, value } = e.target;
        setForms({
            ...forms,
            [name]: value
        });
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
                
        const response = await fetch('/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username: forms.username, password: forms.password }),
        });
        const body = await response.text();
        let { isAuth } = JSON.parse(body);
             
        if(isAuth) { 
            setIsAuth(true);
            localStorage.setItem('isAuth', true);
        }
        else{ 
            alert('Usuário e/ou senha incorretos.');
            setIsAuth(false);
            localStorage.setItem('isAuth', false);
        }
    }

    return (
        <div className={styles.login}>
            <form className={styles.container} onSubmit={(e) => handleSubmit(e)}>
                <div className={styles.inputGroup}>
                    <span>Usuário:</span>
                    <input type="text" id="username" name="username" onChange={(e) => handleChange(e)}/>
                </div>
                <div className={styles.inputGroup}>
                    <span>Senha:</span>
                    <input type="password" id="password" name="password" onChange={(e) => handleChange(e)}/>
                </div>
                <div className={styles.inputGroup}>
                    <button type="submit" id="loginButton" name="loginButton">
                        Entrar
                    </button>
                </div>
            </form>
        </div>
    );
}

export default Login;