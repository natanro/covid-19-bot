import unittest

from src.models.comments import Comment
from src.comments_system.comments_lang_processsor import CommentsLanguageProcessor


class CommentsLanguageProcessorTest(unittest.TestCase):

    def setUp(self):
        self.comments = {}
        self.marca = "marca"

    def test_classe_equivalencia_invalida(self):
        self.comments['text'] = 1
        comm = Comment(self.comments)
        lang_proc = CommentsLanguageProcessor(comm, self.marca)
        ret = lang_proc.comments_vectorized()
        self.assertEqual(ret, None)

    def test_classe_equivalencia_valida(self):
        '''
        é possivel receber um conjunto de comentarios ou apenas 1 comentário
        que o processamento de linguagem natural deve devolver o mesmo vetor
        de palavras de acordo com o tradutor construído para cada marca.
        '''
        self.comments['text'] = "oi mundo"
        listComments = {'text': ["oi mundo"]}
        comm_str = Comment(self.comments)
        comm_list = Comment(listComments)
        ret_str = CommentsLanguageProcessor(
            comm_str, self.marca).comments_vectorized()
        print('saida "oi mundo": '+str(ret_str))
        ret_list = CommentsLanguageProcessor(
            comm_list, self.marca).comments_vectorized()
        self.assertEqual(ret_str, ret_list)

    
